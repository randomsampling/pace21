#include <algorithm>
#include <random>
#include "../input.h"
using namespace std;
vector<vector<int> > input, current; 
void insert_edge(vector<vector<int> > &adj, int a, int b) {
	adj[a].push_back(b);
	adj[b].push_back(a);
}

/* find an augmenting path which increases the matching size */
int augment(int a, vector<int> &matched, vector<int> &seen, int upto) {
	if (seen[a] == upto) return 0;
	seen[a] = upto;
	for (int b : input[a]) {
		if (b == matched[a]) continue;
		if (!matched[b] && seen[b] != upto) {
			matched[b] = a;
			matched[a] = b;
			return 1;
		}
		if (matched[b] > 0) {
			seen[b] = upto;
			int result = augment(matched[b], matched, seen, upto);
			if (result) {
				matched[a] = b;
				matched[b] = a;
				return 1;
			}
		}
	}
	return 0;
}

int main() {
	input = get_adj_list();
	current = vector<vector<int> >(input.size(), vector<int>());

	mt19937 rng;
	int n = (int)input.size()-1;

	/* store all the edges in one vector */
	vector<pair<int, int> > edge_list;
	for (int i = 1; i <= n; i++) {
		for (int a : input[i]) {
			if (i < a) edge_list.push_back({i, a});
		}
	}

	/* process the edges in a random order */
	shuffle(edge_list.begin(), edge_list.end(), rng);

	vector<int> matched(n+1, 0);
	vector<int> seen(n+1, 0);

	int sz = 0;
	int upto = 0;

	/* run matching algorithm */
	int num = 0;
	while (1) {
		fprintf(stderr, "starting new iteration... %d - num %d\n", upto, num);
		int found_match = 0;
		upto++;
		for (int i = 1; i <= n; i++) {
			if (matched[i]) continue;
			int res = augment(i, matched, seen, upto);
			if (res == 1) {
				num++;
				found_match = 1;
			}
		}
		if (!found_match) break;
	}
	
	for (int i = 1; i <= n; i++) {
		if (matched[i] > i) {
			insert_edge(current, i, matched[i]);
			sz++;
		}
	}

	printf("%d\n", (int)edge_list.size()-sz);
	//print_from_adj_list(input, current);
}