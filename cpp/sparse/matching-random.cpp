#include <algorithm>
#include <random>
#include "../input.h"
using namespace std;
vector<vector<int> > input, current; 
int main() {
	input = get_adj_list();
	current = vector<vector<int> >(input.size(), vector<int>());

	mt19937 rng;
	int n = (int)input.size()-1;

	/* store all the edges in one vector */
	vector<pair<int, int> > edge_list;
	for (int i = 1; i <= n; i++) {
		for (int a : input[i]) {
			if (i < a) edge_list.push_back({i, a});
		}
	}

	/* process the edges in a random order */
	shuffle(edge_list.begin(), edge_list.end(), rng);

	/* take an edge if both endpoints are unmatched */
	vector<bool> matched(n+1, false);
	int sz = 0;
	for (auto e : edge_list) {
		if (!matched[e.first] && !matched[e.second]) {
			matched[e.first] = matched[e.second] = true;
			current[e.first].push_back(e.second);
			current[e.second].push_back(e.first);
			sz++;
		}
	}

	printf("%d\n", (int)edge_list.size()-sz);
	//print_from_adj_list(input, current);
}