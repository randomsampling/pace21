#include <algorithm>
#include <random>
#include "../input.h"
using namespace std;
vector<vector<int> > inputList, currentList; 
void insert_edge(vector<vector<int> > &adj, int a, int b) {
	adj[a].push_back(b);
	adj[b].push_back(a);
}

/* find an augmenting path which increases the matching size */
int augment(int a, vector<int> &matched, vector<int> &seen, int upto) {
	if (seen[a] == upto) return 0;
	seen[a] = upto;
	for (int b : inputList[a]) {
		if (b == matched[a]) continue;
		if (!matched[b] && seen[b] != upto) {
			matched[b] = a;
			matched[a] = b;
			return 1;
		}
		if (matched[b] > 0) {
			seen[b] = upto;
			int result = augment(matched[b], matched, seen, upto);
			if (result) {
				matched[a] = b;
				matched[b] = a;
				return 1;
			}
		}
	}
	return 0;
}

int main() {
	inputList = get_adj_list();
	currentList = vector<vector<int> >(inputList.size(), vector<int>());

	mt19937 rng;
	int n = (int)inputList.size()-1;

	/* store all the edges in one vector */
	vector<pair<int, int> > edge_list;
	for (int i = 1; i <= n; i++) {
		for (int a : inputList[i]) {
			if (i < a) edge_list.push_back({i, a});
		}
	}

	/* sort so we can binary search on it */
	sort(edge_list.begin(), edge_list.end());

	vector<int> matched(n+1, 0);
	int sz = 0;

	/* take triangles if we can */
	for (auto e : edge_list) {
		if (!matched[e.first] && !matched[e.second]) {
			for (int a : inputList[e.first]) {
				/* check if the triangle e.first, e.second, a is a viable option */
				if (!matched[a] && a != e.second) {
					if (binary_search(edge_list.begin(), edge_list.end(), make_pair(min(e.second, a), max(e.second, a)))) {
						matched[a] = matched[e.first] = matched[e.second] = -1;
						insert_edge(currentList, a, e.first);
						insert_edge(currentList, a, e.second);
						insert_edge(currentList, e.first, e.second);
						sz+=3;
					}
				}
				break;
			}
		}
	}

	/* run matching algorithm */
	int upto = 0, num = 0;
	vector<int> seen(n+1, 0);
	while (1) {
		fprintf(stderr, "starting new iteration... %d - num %d\n", upto, num);
		int found_match = 0;
		upto++;
		for (int i = 1; i <= n; i++) {
			if (matched[i]) continue;
			int res = augment(i, matched, seen, upto);
			if (res == 1) {
				num++;
				found_match = 1;
			}
		}
		if (!found_match) break;
	}

	int score = (int)edge_list.size()-sz-num;


	printf("%d\n", (int)edge_list.size()-sz-num);
	//print_from_adj_list(inputList, currentList);
}