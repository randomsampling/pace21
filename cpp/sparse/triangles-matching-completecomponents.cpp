#include <algorithm>
#include <random>
#include "../input.h"
using namespace std;
vector<vector<int> > input, current; 
void insert_edge(vector<vector<int> > &adj, int a, int b) {
	adj[a].push_back(b);
	adj[b].push_back(a);
}

/* find an augmenting path which increases the matching size */
int augment(int a, vector<int> &matched, vector<int> &seen, int upto) {
	if (seen[a] == upto) return 0;
	seen[a] = upto;
	for (int b : input[a]) {
		if (b == matched[a]) continue;
		if (!matched[b] && seen[b] != upto) {
			matched[b] = a;
			matched[a] = b;
			return 1;
		}
		if (matched[b] > 0 && seen[b] != upto) {
			seen[b] = upto;
			int result = augment(matched[b], matched, seen, upto);
			if (result) {
				matched[a] = b;
				matched[b] = a;
				return 1;
			}
		}
	}
	return 0;
}

void find_component(int a, vector<vector<int> > &adj, vector<int> &seen, int upto, vector<int> &component) {
	if (seen[a] == upto) return;
	seen[a] = upto;
	component.push_back(a);
	for (int b : adj[a]) {
		find_component(b, adj, seen, upto, component);
	}
}

int main() {
	input = get_adj_list();
	current = vector<vector<int> >(input.size(), vector<int>());

	mt19937 rng;
	int n = (int)input.size()-1;

	/* store all the edges in one vector */
	vector<pair<int, int> > edge_list;
	for (int i = 1; i <= n; i++) {
		for (int a : input[i]) {
			if (i < a) edge_list.push_back({i, a});
		}
	}

	/* sort so we can binary search on it */
	sort(edge_list.begin(), edge_list.end());

	vector<int> matched(n+1, 0);
	int sz = 0;

	/* take triangles if we can */
	for (auto e : edge_list) {
		if (!matched[e.first] && !matched[e.second]) {
			for (int a : input[e.first]) {
				/* check if the triangle e.first, e.second, a is a viable option */
				if (!matched[a] && a != e.second) {
					if (binary_search(edge_list.begin(), edge_list.end(), make_pair(min(e.second, a), max(e.second, a)))) {
						matched[a] = matched[e.first] = matched[e.second] = -1;
						insert_edge(current, a, e.first);
						insert_edge(current, a, e.second);
						insert_edge(current, e.first, e.second);
						sz+=3;
						break;
					}
				}
				if (matched[e.first]) break;
			}
		}
	}

	/* run matching algorithm */
	int upto = 0, num = 0;
	vector<int> seen(n+1, 0);
	while (1) {
		fprintf(stderr, "starting new matching iteration... %d - num %d\n", upto, num);
		int found_match = 0;
		upto++;
		for (int i = 1; i <= n; i++) {
			if (matched[i]) continue;
			int res = augment(i, matched, seen, upto);
			if (res == 1) {
				num++;
				found_match = 1;
			}
		}
		if (!found_match) break;
	}

	for (int i = 1; i <= n; i++) {
		if (matched[i] > i) insert_edge(current, i, matched[i]);
	}

	/* check for each component, would it be better if we just made it into a clique */
	upto++;
	for (int i = 1; i <= n; i++) {
		if (seen[i] != upto) {
			/* dfs to find the component */
			vector<int> component;
			find_component(i, input, seen, upto, component);
			/* count the number of edges in that component */
			int num_edges_in = 0, num_edges_not_in = 0;
			for (int a : component) {
				for (int b : current[a]) {
					if (b > a) num_edges_in++;
				}
				for (int b : input[a]) {
					if (b > a) num_edges_not_in++;
				}
			}
			num_edges_not_in -= num_edges_in;

			//fprintf(stderr, "found component with %d nodes and %d edges in and %d edges not in\n", (int)component.size(), num_edges_in, num_edges_not_in);
			long long edges_if_complete = component.size();
			edges_if_complete *= (edges_if_complete-1);
			edges_if_complete /= 2;
			if (edges_if_complete-(num_edges_in + num_edges_not_in) < num_edges_not_in) {
				// we should make this component a clique
				int score_improvement =  2*num_edges_not_in + num_edges_in - edges_if_complete;
				fprintf(stderr, "making clique, with improves score by %d\n", score_improvement);
				for (int a : component) {
					current[a].clear();
					for (int b : component) {
						if (a != b) current[a].push_back(b);
					}
				}
			}
		}

	}

	fprintf(stderr, "%d\n", (int)edge_list.size()-sz-num);
		
	/* todo: fix below */
	if (n <= 5000) {
		adj_matrix = vector<vector<bool> >(input.size(), vector<bool>(input.size(), 0));
		for (int i = 1; i <= n; i++) {
			for (auto a : current[i]) {
				adj_matrix[i][a] = adj_matrix[a][i] = 1;
			}
		}
		random_improve(input, adj_matrix);
	} else {
		print_from_adj_list(input, current);
	}
}