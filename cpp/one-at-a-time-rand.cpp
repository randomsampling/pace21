/* 
Written by Angus

Algorithm description:
- Process each node one at a time
- Add them to the component (or leave them isolated) such that changes are minimised
- O(n)
*/
#include "input.h"
#include <algorithm>
#include <random>
#include <chrono>
using namespace std;


vector<vector<int> > input, oaat_currentList;
int n;
int oaatScore = 1e9;

int cost(int a, const vector<int> &comp, const vector<int> &inorder) {
	// assumes comp is sorted
	int edges_incomp = 0, ans = 0;
	for (int b : input[a]) {
		if (inorder[b] < inorder[a]) {
			if (binary_search(comp.begin(), comp.end(), b)) {
				edges_incomp++;
			} else {
				ans++;
			}
		}
	}
	ans += (int)comp.size() - edges_incomp;
	return ans;
}

void insert_edge(vector<vector<int> > &adj, int a, int b) {
	adj[a].push_back(b);
	adj[b].push_back(a);
}

int count_difference(vector<vector<int> > initial, vector<vector<int> > final) {
	// Returns the number of edge differences between the two graphs
	int diff = 0;
	int n = initial.size()-1;
	for (int i = 1; i <= n; i++) {
		sort(initial[i].begin(), initial[i].end());
		sort(final[i].begin(), final[i].end());
		for (int a : initial[i]) {
			diff += !binary_search(final[i].begin(), final[i].end(), a);
		}
		for (int a : final[i]) {
			diff += !binary_search(initial[i].begin(), initial[i].end(), a);
		}
	}
	return diff/2;
}

int main() {

	mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

	// Start the timer
	// We will run for around at most a minute
	chrono::milliseconds start = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());


	input = get_adj_list();
	n = (int)input.size()-1;

	// Run a bunch of times with different random orderings
	while (1) {
		vector<vector<int> > components;

		vector<int> incomp(n+1);
		vector<int> edgesToComp(n+1);

		// Create a random ordering of the vertices
		vector<int> order;
		for (int i = 0; i <= n; i++) order.push_back(i);
	    shuffle(order.begin()+1, order.end(), rng);

		vector<int> inorder(n+1);
	    for (int i = 1; i <= n; i++) inorder[order[i]] = i;


		for (int f = 1; f <= n; f++) {
			int i = order[f];
			// We ignore edges which don't go to a node we have already processed
			int backwards_edges = 0;
			for (int a : input[i]) {
				backwards_edges += inorder[a] < inorder[i];
			}

			// Initialise the count for each component
			for (int a : input[i]) {
				if (inorder[a] < inorder[i]) {
					edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + backwards_edges;
				}
			}

			// Count the number of edges to each component and find the best component
			pair<int, int> best = { backwards_edges, components.size() };
			for (int a : input[i]) {
				if (inorder[a] < inorder[i]) {
					edgesToComp[incomp[a]] -= 2;
					best = min(best, { edgesToComp[incomp[a]], incomp[a] });
				}
			}
			
			if (best.second == components.size()) assert(best.first == cost(i, vector<int>(), inorder));
			else {
				assert(best.first == cost(i, components[best.second], inorder));
			}

			if (best.second == components.size()) {
				vector<int> v = { i };
				components.push_back(v);
			} else {
				components[best.second].push_back(i);
				sort(components[best.second].begin(), components[best.second].end());
			}
			incomp[i] = best.second;

		}
		// now, swapping phase
		for (int num = 0; num < 10; num++) {
			for (int f = 1; f <= n; f++) {
				int i = order[f];

				for (int a : input[i]) {
					edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + input[i].size();
				}
				edgesToComp[incomp[i]] = ((int)components[incomp[i]].size())-1 + input[i].size();

				// Count the number of edges to each component and find the best component
				pair<int, int> best = { edgesToComp[incomp[i]], incomp[i] };
				for (int a : input[i]) {
					edgesToComp[incomp[a]] -= 2;
					best = min(best, { edgesToComp[incomp[a]], incomp[a] });
				}
				if (incomp[i] != best.second) {
					auto it = find(components[incomp[i]].begin(), components[incomp[i]].end(), i);
					assert(it != components[incomp[i]].end());
					components[incomp[i]].erase(it);
					incomp[i] = best.second;
					components[incomp[i]].push_back(i);
				}
			}
		}
		vector<vector<int> > current = vector<vector<int> >(n+1);
		for (auto c :  components) {
			for (auto a : c) {
				for (auto b : c) {
					if (a < b) insert_edge(current, a, b);
				}
			}
		}

		int score = count_difference(input, current);
		if (score < oaatScore) {
			fprintf(stderr, "new best %d\n", score);
			oaatScore = score;
			oaat_currentList = current;
		}

		chrono::milliseconds end = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
		long long totaltime = end.count() - start.count();

		if (totaltime > 60000) break; // 10 seconds
	}

	fprintf(stderr, "final difference %d\n", count_difference(input, oaat_currentList));
	//print_from_adj_list(input, oaat_currentList);
}
