/* 
Written by Angus

Same as random greedy, but with a better time complexity. Does this by only considering edges that are part of a bad set

Algorithm description:
- Call a set of 3 nodes bad if there are 2 edges between them
- For each possible edge, try adding or removing it, and see what it does to the number of good/bad sets
- Add/remove the edge which decreases the number of bad sets by the most
*/
#include "input.h"
#include "random-improve.h"
#include <algorithm>
#include <utility>
#include <random>
#include <set>
using namespace std;
typedef long long ll;

vector<vector<bool> > input, current; 
int n;


set<pair<int, int> > bad_edges; // an edge is bad if it is in a bad set. Includes edges which are not in the graph

// Counts the number of bad sets which contain these two nodes. If change_set is true, adds bad edges to the bad_edges set
int count_bad(int a, int b, bool change_set = false) {
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		if (i != a && i != b) {
			if ((int)current[a][b] + (int)current[a][i] + (int)current[b][i] == 2) {
				ans++;
				if (change_set) {
					bad_edges.insert({min(a, b), max(a, b)});
					bad_edges.insert({min(a, i), max(a, i)});
					bad_edges.insert({min(b, i), max(b, i)});
				}
			}
		}
	}
	return ans;
}

ll total_bad;


ll count_total_bad() {
	ll tot = 0;
	bad_edges.clear();
	for (int i = 1; i <= n; i++) {
		for (int j = i+1; j <= n; j++) {
			for (int k = j+1; k <= n; k++) {
				if ((int)current[i][j] + (int)current[i][k] + (int)current[j][k] == 2) {
					tot++;
					bad_edges.insert({i, j});
					bad_edges.insert({j, k});
					bad_edges.insert({i, k});
				}
			}
		}
	}
	return tot;
}


int main() {
	input = current = get_adj_matrix();
	n = (int)input.size()-1;

	total_bad = count_total_bad();

	for (int count = 0; total_bad != 0; count++) {
		vector<pair<int, pair<int, int> > > edges; // .first the change in bad sets if we change the state of the edge. .second is the edge
		for (auto e : bad_edges) {
			int i = e.first, j = e.second;

			int b  = count_bad(i, j);
			current[i][j] = !current[i][j]; current[j][i] = !current[j][i];
			int new_b = count_bad(i, j);
			current[i][j] = !current[i][j]; current[j][i] = !current[j][i];

			int diff = new_b-b;
			edges.push_back({diff, {i, j}});
		}
		sort(edges.begin(), edges.end());
		int index = 0;
		pair<int, int> edge = edges[index].second;
		current[edge.first][edge.second] = !current[edge.first][edge.second];
		current[edge.second][edge.first] = !current[edge.second][edge.first];

		count_bad(edge.first, edge.second, true);

		total_bad += edges[index].first;

		fprintf(stderr, "edge: %d %d, new score %lld\n", edge.first, edge.second, total_bad);

		// recompute bad_edges every so often, to remove any edges which are not bad anymore
		if (count%n == 0) {
			count_total_bad();
		}
	}

	random_improve(input, current);
}
