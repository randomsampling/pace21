/* 
Written by Angus

Algorithm description:
- Call a set of 3 nodes bad if there are 2 edges between them
- For each possible edge, try adding or removing it, and see what it does to the number of good/bad sets
- Add/remove the edge which decreases the number of bad sets by the most
*/
#include "input.h"
#include <algorithm>
#include <utility>
#include <random>
using namespace std;
typedef long long ll;

vector<vector<bool> > input, current; 
int n;

// Counts the number of bad sets which contain these two nodes
int count_bad(int a, int b) {
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		if (i != a && i != b) {
			ans += (int)current[a][b] + (int)current[a][i] + (int)current[b][i] == 2;
		}
	}
	return ans;
}

ll total_bad;

ll count_total_bad() {
	ll tot = 0;
	
	for (int i = 1; i <= n; i++) {
		for (int j = i+1; j <= n; j++) {
			for (int k = j+1; k <= n; k++) {
				tot += (int)current[i][j] + (int)current[i][k] + (int)current[j][k] == 2;
			}
		}
	}
	return tot;
}

int main() {
	input = current = get_adj_matrix();
	n = (int)input.size()-1;

	total_bad = count_total_bad();

	while (total_bad) {
		vector<pair<int, pair<int, int> > > edges; // .first the change in bad sets if we change the state of the edge. .second is the edge
		for (int i = 1; i <= n; i++) {
			for (int j = i+1; j <= n; j++) {
				int b  = count_bad(i, j);
				current[i][j] = !current[i][j]; current[j][i] = !current[j][i];
				int new_b = count_bad(i, j);
				current[i][j] = !current[i][j]; current[j][i] = !current[j][i];

				int diff = new_b-b;
				edges.push_back({diff, {i, j}});
			}
		}
		sort(edges.begin(), edges.end());
		int index = 0;
		pair<int, int> edge = edges[index].second;
		current[edge.first][edge.second] = !current[edge.first][edge.second];
		current[edge.second][edge.first] = !current[edge.second][edge.first];

		total_bad += edges[index].first;

		fprintf(stderr, "edge: %d %d, new score %lld\n", edge.first, edge.second, total_bad);
	}

	print_from_adj_matrix(input, current);
}
