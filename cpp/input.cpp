#include "input.h"
#include <algorithm>
using namespace std;
/*
Scans in a graph and returns it as an adjancy matrix, where 1 represents an edge.
Uses 1-indexing, so index 0 should be ignored 
Uses this input format https://pacechallenge.org/2021/tracks/#input-format
Note: current does not consider comments
*/
std::vector<std::vector<bool> > get_adj_matrix() {
	int n, m;
	scanf("p cep %d%d", &n, &m);
	vector<vector<bool> > adj(n+1, vector<bool>(n+1, 0));
	for (int i = 0; i < m; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a][b] = adj[b][a] = true;
	}
	return adj;
}


/*
Scans in a graph and returns it as an adjancy list. In particular, index i in the returned vector contains the edges adjacent to i, in sorted order
Uses 1-indexing, so index 0 should be ignored
Uses this input format https://pacechallenge.org/2021/tracks/#input-format 
*/
std::vector<std::vector<int> > get_adj_list() {
	int n, m;
	scanf("p cep %d%d", &n, &m);
	vector<vector<int> > adj(n+1, vector<int>(0));
	for (int i = 0; i < m; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
		adj[b].push_back(a);
	}
	for (int i = 1; i <= n; i++) {
		sort(adj[i].begin(), adj[i].end());
	}
	return adj;
}


/*
Takes in two adjacency matrices (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_matrix(std::vector<std::vector<bool> > initial, std::vector<std::vector<bool> > final) {
	for (int i = 1; i < (int)initial.size(); i++) {
		for (int j = i+1; j < (int)initial.size(); j++) {
			if (initial[i][j] != final[i][j]) {
				printf("%d %d\n", i, j);
			}
		}
	}
}

/*
Takes in two adjacency lists (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_list(std::vector<std::vector<int> > initial, std::vector<std::vector<int> > final) {
	for (int i = 1; i < (int)initial.size(); i++) {
		sort(initial[i].begin(), initial[i].end());
		sort(final[i].begin(), final[i].end());
		for (int a : initial[i]) {
			if (i < a && !binary_search(final[i].begin(), final[i].end(), a)) {
				printf("%d %d\n", i, a);
			}
		}
		for (int a : final[i]) {
			if (i < a && !binary_search(initial[i].begin(), initial[i].end(), a)) {
				printf("%d %d\n", i, a);
			}
		}
	}
}