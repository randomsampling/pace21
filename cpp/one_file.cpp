/* Everything has been put in one file */

/* First is input.h */

/* 
Some functions to help us with input
Functions are implemented in input.cpp
*/

#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>
#include <random>
#include <csignal>
#include <set>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <chrono>
#ifdef DEBUG
    #define D(x...) fprintf(stderr, x)
#else
    #define D(x...)
#endif
using namespace std;
typedef long long ll;

/*
Scans in a graph and returns it as an adjancy matrix, where 1 represents an edge.
Uses 1-indexing, so index 0 should be ignored 
Uses this input format https://pacechallenge.org/2021/tracks/#input-format
Note: current does not consider comments
*/
std::vector<std::vector<bool> > get_adj_matrix();


/*
Scans in a graph and returns it as an adjancy list. In particular, index i in the returned vector contains the edges adjacent to i, in sorted order
Uses 1-indexing, so index 0 should be ignored
Uses this input format https://pacechallenge.org/2021/tracks/#input-format 
*/
std::vector<std::vector<int> > get_adj_list();


/*
Takes in two adjacency matrices (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_matrix(std::vector<std::vector<bool> > initial, std::vector<std::vector<bool> > final);


/* Here is input.cpp */

/*
Scans in a graph and returns it as an adjancy matrix, where 1 represents an edge.
Uses 1-indexing, so index 0 should be ignored 
Uses this input format https://pacechallenge.org/2021/tracks/#input-format
Note: current does not consider comments
*/
std::vector<std::vector<bool> > get_adj_matrix() {
    int n, m;
    scanf("p cep %d%d", &n, &m);
    vector<vector<bool> > adj(n+1, vector<bool>(n+1, 0));
    for (int i = 0; i < m; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a][b] = adj[b][a] = true;
    }
    return adj;
}


/*
Scans in a graph and returns it as an adjancy list. In particular, index i in the returned vector contains the edges adjacent to i, in sorted order
Uses 1-indexing, so index 0 should be ignored
Uses this input format https://pacechallenge.org/2021/tracks/#input-format 
*/
std::vector<std::vector<int> > get_adj_list() {
    int n, m;
    scanf("p cep %d%d", &n, &m);
    vector<vector<int> > adj(n+1, vector<int>(0));
    for (int i = 0; i < m; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    for (int i = 1; i <= n; i++) {
        sort(adj[i].begin(), adj[i].end());
    }
    return adj;
}


/*
Takes in two adjacency matrices (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_matrix(std::vector<std::vector<bool> > initial, std::vector<std::vector<bool> > final) {
    for (int i = 1; i < (int)initial.size(); i++) {
        for (int j = i+1; j < (int)initial.size(); j++) {
            if (initial[i][j] != final[i][j]) {
                printf("%d %d\n", i, j);
            }
        }
    }
}


/*
Returns the number of edge differences between the two graphs
*/
int count_difference(vector<vector<int> > initial, vector<vector<int> > final) {
    int diff = 0;
    int n = initial.size()-1;
    for (int i = 1; i <= n; i++) {
        sort(initial[i].begin(), initial[i].end());
        sort(final[i].begin(), final[i].end());
        for (int a : initial[i]) {
            diff += !binary_search(final[i].begin(), final[i].end(), a);
        }
        for (int a : final[i]) {
            diff += !binary_search(initial[i].begin(), initial[i].end(), a);
        }
    }
    return diff/2;
}

/*
Takes in two adjacency lists (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_list(std::vector<std::vector<int> > initial, std::vector<std::vector<int> > final) {
    for (int i = 1; i < (int)initial.size(); i++) {
        sort(initial[i].begin(), initial[i].end());
        sort(final[i].begin(), final[i].end());
        for (int a : initial[i]) {
            if (i < a && !binary_search(final[i].begin(), final[i].end(), a)) {
                printf("%d %d\n", i, a);
            }
        }
        for (int a : final[i]) {
            if (i < a && !binary_search(initial[i].begin(), initial[i].end(), a)) {
                printf("%d %d\n", i, a);
            }
        }
    }
}


vector<vector<int> > inputList, currentList, oaat_currentList;
void insert_edge(vector<vector<int> > &adj, int a, int b) {
    adj[a].push_back(b);
    adj[b].push_back(a);
}

/* find an augmenting path which increases the matching size */
int augment(int a, vector<int> &matched, vector<int> &seen, int upto) {
    if (seen[a] == upto) return 0;
    seen[a] = upto;
    for (int b : inputList[a]) {
        if (b == matched[a]) continue;
        if (!matched[b] && seen[b] != upto) {
            matched[b] = a;
            matched[a] = b;
            return 1;
        }
        if (matched[b] > 0) {
            seen[b] = upto;
            int result = augment(matched[b], matched, seen, upto);
            if (result) {
                matched[a] = b;
                matched[b] = a;
                return 1;
            }
        }
    }
    return 0;
}

int triangleScore = 1e9, oaatScore = 1e9;

volatile int tle = 0;
 
void term(int signum)
{
    tle = 1;
}

vector<vector<int> > swapping_phase(vector<vector<int> > &components, vector<int> &incomp, const vector<vector<int> > &input, int num_iterations, const vector<int> order);

void oaat();

int main() {

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);


    inputList = get_adj_list();
    currentList = vector<vector<int> >(inputList.size(), vector<int>());

    mt19937 rng;
    int n = (int)inputList.size()-1;

    /* store all the edges in one vector */
    vector<pair<int, int> > edge_list;
    for (int i = 1; i <= n; i++) {
        for (int a : inputList[i]) {
            if (i < a) edge_list.push_back({i, a});
        }
    }

    /* sort so we can binary search on it */
    sort(edge_list.begin(), edge_list.end());

    vector<int> matched(n+1, 0);
    int sz = 0;

    vector<vector<int> > components;
    /* take triangles if we can */
    for (auto e : edge_list) {
        if (!matched[e.first] && !matched[e.second]) {
            for (int a : inputList[e.first]) {
                /* check if the triangle e.first, e.second, a is a viable option */
                if (!matched[a] && a != e.second) {
                    if (binary_search(edge_list.begin(), edge_list.end(), make_pair(min(e.second, a), max(e.second, a)))) {
                        matched[a] = matched[e.first] = matched[e.second] = -1;
                        components.push_back({a, e.first, e.second});
                        sz+=3;
                        break;
                    }
                }
            }
        }
    }

    /* run matching algorithm */
    int upto = 0, num = 0;
    vector<int> seen(n+1, 0);
    while (!tle) {
        D("starting new iteration... %d - num %d\n", upto, num);
        int found_match = 0;
        upto++;
        for (int i = 1; i <= n; i++) {
            if (matched[i]) continue;
            int res = augment(i, matched, seen, upto);
            if (res == 1) {
                num++;
                found_match = 1;
            }
        }
        if (!found_match) break;
    }

    for (int i = 1; i <= n; i++) {
        if (matched[i] > i) {
            components.push_back({i, matched[i]});
        } else if (!matched[i]) {
            components.push_back({i});
        }
    }

    triangleScore = (int)edge_list.size()-sz-num;
    D("original triangle score %d\n", triangleScore);

    vector<int> incomp(n+1);
    for (int i = 0; i < (int)components.size(); i++) {
        for (auto a : components[i]) incomp[a] = i;
    }

    vector<int> order(n+1);
    iota(order.begin(), order.end(), 0);
    currentList = swapping_phase(components, incomp, inputList, 10, order);

    triangleScore = count_difference(currentList, inputList);

    D("new triangle score %d\n", triangleScore);

    /* Run one at a time */
    oaat();

    if (triangleScore <= oaatScore) {
        print_from_adj_list(inputList, currentList);
    } else {
        print_from_adj_list(inputList, oaat_currentList);
    }
}


/* 
one-at-a-time-rand-fast

Written by Angus

Algorithm description:
- Process each node one at a time, in a random order
- Add them to the component (or leave them isolated) such that changes are minimised
- O(n)
*/


void oaat() {

    mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


    int n = (int)inputList.size()-1;


    // Run a bunch of times with different random orderings

    while (!tle) {
        vector<vector<int> > components;

        vector<int> incomp(n+1);
        vector<int> edgesToComp(n+1);

        // Create a random ordering of the vertices
        vector<int> order;
        for (int i = 0; i <= n; i++) order.push_back(i);
        shuffle(order.begin()+1, order.end(), rng);

        vector<int> inorder(n+1);
        for (int i = 1; i <= n; i++) inorder[order[i]] = i;


        for (int f = 1; f <= n; f++) {
            int i = order[f];
            // We ignore edges which don't go to a node we have already processed
            int backwards_edges = 0;
            for (int a : inputList[i]) {
                backwards_edges += inorder[a] < inorder[i];
            }

            // Initialise the count for each component
            for (int a : inputList[i]) {
                if (inorder[a] < inorder[i]) {
                    edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + backwards_edges;
                }
            }

            // Count the number of edges to each component and find the best component
            pair<int, int> best = { backwards_edges, components.size() };
            for (int a : inputList[i]) {
                if (inorder[a] < inorder[i]) {
                    edgesToComp[incomp[a]] -= 2;
                    best = min(best, { edgesToComp[incomp[a]], incomp[a] });
                }
            }

            if (best.second == components.size()) {
                vector<int> v = { i };
                components.push_back(v);
            } else {
                components[best.second].push_back(i);
            }
            incomp[i] = best.second;
        }

        // now, swapping phase
        for (int num = 0; num < 10; num++) {
            for (int f = 1; f <= n; f++) {
                int i = order[f];

                for (int a : inputList[i]) {
                    edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + inputList[i].size();
                }
                edgesToComp[incomp[i]] = ((int)components[incomp[i]].size())-1 + inputList[i].size();

                // Count the number of edges to each component and find the best component
                pair<int, int> best = { edgesToComp[incomp[i]], incomp[i] };
                for (int a : inputList[i]) {
                    edgesToComp[incomp[a]] -= 2;
                    best = min(best, { edgesToComp[incomp[a]], incomp[a] });
                }
                if (incomp[i] != best.second) {
                    auto it = find(components[incomp[i]].begin(), components[incomp[i]].end(), i);
                    assert(it != components[incomp[i]].end());
                    components[incomp[i]].erase(it);
                    incomp[i] = best.second;
                    components[incomp[i]].push_back(i);
                }
            }
        }
        vector<vector<int> > current = swapping_phase(components, incomp, inputList, 10, order);

        int score = count_difference(inputList, current);

        if (score < oaatScore) {
            D("new best %d\n", score);
            oaatScore = score;
            oaat_currentList = current;
        }
    }
}


/*
Tries moving vertices from one component to another, if it improves the graph
Returns an adj list for the new graph
*/
vector<vector<int> > swapping_phase(vector<vector<int> > &components, vector<int> &incomp, const vector<vector<int> > &input, int num_iterations, const vector<int> order) {
    int n = (int)input.size()-1;
    vector<int> edgesToComp(n+1);
    for (int num = 0; num < num_iterations; num++) {
        for (int f = 1; f <= n; f++) {
            int i = order[f];

            for (int a : inputList[i]) {
                edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + inputList[i].size();
            }
            edgesToComp[incomp[i]] = ((int)components[incomp[i]].size())-1 + inputList[i].size();

            // Count the number of edges to each component and find the best component
            pair<int, int> best = { edgesToComp[incomp[i]], incomp[i] };
            for (int a : inputList[i]) {
                edgesToComp[incomp[a]] -= 2;
                best = min(best, { edgesToComp[incomp[a]], incomp[a] });
            }
            if (incomp[i] != best.second) {
                auto it = find(components[incomp[i]].begin(), components[incomp[i]].end(), i);
                assert(it != components[incomp[i]].end());
                components[incomp[i]].erase(it);
                incomp[i] = best.second;
                components[incomp[i]].push_back(i);
            }
        }
    }

    vector<vector<int> > current(n+1);
    for (auto c :  components) {
        for (auto a : c) {
            for (auto b : c) {
                if (a < b) insert_edge(current, a, b);
            }
        }
    }
    return current;
}