/* 
Written by Angus

Algorithm description:
- Process each node one at a time
- Add them to the component (or leave them isolated) such that changes are minimised
- O(n)
*/
#include "input.h"
#include <algorithm>
using namespace std;


vector<vector<int> > input, current; 
int n;

int cost(int a, const vector<int> &comp) {
	// assumes comp is sorted
	int edges_incomp = 0, ans = 0;
	for (int b : input[a]) {
		if (b < a) {
			if (binary_search(comp.begin(), comp.end(), b)) {
				edges_incomp++;
			} else {
				ans++;
			}
		}
	}
	ans += (int)comp.size() - edges_incomp;
	return ans;
}

void insert_edge(vector<vector<int> > &adj, int a, int b) {
	adj[a].push_back(b);
	adj[b].push_back(a);
}

int count_difference(vector<vector<int> > initial, vector<vector<int> > final) {
	// Returns the number of edge differences between the two graphs
	int diff = 0;
	int n = initial.size()-1;
	for (int i = 1; i <= n; i++) {
		sort(initial[i].begin(), initial[i].end());
		sort(final[i].begin(), final[i].end());
		for (int a : initial[i]) {
			diff += !binary_search(final[i].begin(), final[i].end(), a);
		}
		for (int a : final[i]) {
			diff += !binary_search(initial[i].begin(), initial[i].end(), a);
		}
	}
	return diff/2;
}

int main() {
	input = get_adj_list();
	n = (int)input.size()-1;
	current = vector<vector<int> >(n+1);
	vector<vector<int> > components;
	vector<int> incomp(n+1);
	vector<int> edgesToComp(n+1);

	for (int i = 1; i <= n; i++) {
		// We ignore edges which don't go to a node we have already processed
		int backwards_edges = 0;
		for (int a : input[i]) {
			backwards_edges += a < i;
		}

		// Initialise the count for each component
		for (int a : input[i]) {
			if (a < i) {
				edgesToComp[incomp[a]] = (int)components[incomp[a]].size() + backwards_edges;
			}
		}

		// Count the number of edges to each component and find the best component
		pair<int, int> best = { backwards_edges, components.size() };
		for (int a : input[i]) {
			if (a < i) {
				edgesToComp[incomp[a]] -= 2;
				best = min(best, { edgesToComp[incomp[a]], incomp[a] });
			}
		}
		
		if (best.second == components.size()) assert(best.first == cost(i, vector<int>()));
		else {
			assert(best.first == cost(i, components[best.second]));
		}


		if (best.second == components.size()) {
			vector<int> v = { i };
			components.push_back(v);
		} else {
			for (int a : components[best.second]) {
				insert_edge(current, i, a);
			}
			components[best.second].push_back(i);
		}
		incomp[i] = best.second;
	}

	fprintf(stderr, "difference %d\n", count_difference(input, current));
	print_from_adj_list(input, current);
}
