/* 
Written by Angus

Algorithm description:
- Takes in the input and an already created cluster graph (that is, every component is already a clique). A graph with no edges is valid input here.
- Randomly does one of two things
  - pick a node. If removing all of its edges (thus removing it from its clique) does not increase the difference between this graph and the input, do it
  - pick two nodes that are not connected. If connecting their components does not increase the difference between this graph and the input, do it
*/
#include "input.h"
#include "random-improve.h"
#include <algorithm>
#include <random>
using namespace std;
typedef long long ll;

int count_difference(vector<vector<bool> > initial, vector<vector<bool> > final) {
	// Returns the number of edge differences between the two graphs
	int diff = 0;
	for (int i = 1; i < (int)initial.size(); i++) {
		for (int j = i+1; j < (int)initial.size(); j++) {
			diff += initial[i][j] != final[i][j]; 
		}
	}
	return diff;
}


void random_improve(vector<vector<bool> > initial, vector<vector<bool> > current) {
	int diff = count_difference(initial, current);
	fprintf(stderr, "random_improve called: currend diff %d\n", diff);
	mt19937 rng;
	int n = (int)initial.size()-1;

	while (1) {
		int olddiff = diff;

		// first, try removing a node
		int a = uniform_int_distribution<int>(1, n)(rng);
		int num_improvements = 0, num_reductions = 0; // num_improvements will be the number of edges that become the same as initial, num_reductions the number which will become not the same
		for (int i = 1; i <= n; i++) {
			if (current[a][i]) {
				num_reductions += initial[a][i];
				num_improvements += !initial[a][i];
			}
		}
		// do the change if it doesn't make our current graph worse
		if (num_improvements >= num_reductions) {
			diff -= num_improvements - num_reductions;
			for (int i = 1; i <= n; i++) {
				if (current[a][i]) {
					current[a][i] = current[i][a] = !current[a][i];
				}
			}
		}	

		// now try merging two nodes
		a = uniform_int_distribution<int>(1, n)(rng);
		int b = uniform_int_distribution<int>(1, n)(rng);
		if (a != b && !current[a][b]) {
			vector<int> comp_a = { a }, comp_b = { b }; // will store the nodes in the same component as a and b respectively
			for (int i = 1; i <= n; i++) {
				if (current[a][i]) comp_a.push_back(i);
				if (current[b][i]) comp_b.push_back(i);
				assert(!current[a][i] || !current[b][i]);
			}

			// calculate num_improvements and reductions as in the previous case
			int num_improvements = 0, num_reductions = 0;
			for (auto i : comp_a) {
				for (auto j : comp_b) {
					assert(!current[i][j]);
					num_reductions += !initial[i][j];
					num_improvements += initial[i][j];
				}
			}

			// do the change if it doesn't make our current graph worse
			if (num_improvements >= num_reductions) {
				diff -= num_improvements - num_reductions;
				for (auto i : comp_a) {
					for (auto j : comp_b) {
						current[i][j] = current[j][i] = !current[i][j];
					}
				}
			}	
		}

		if (diff != olddiff) {
			fprintf(stderr, "New difference: %d\n", diff);
		}
	}
}

