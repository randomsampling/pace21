/* 
Some functions to help us with input
Functions are implemented in input.cpp
*/

#include <cstdio>
#include <vector>

/*
Scans in a graph and returns it as an adjancy matrix, where 1 represents an edge.
Uses 1-indexing, so index 0 should be ignored 
Uses this input format https://pacechallenge.org/2021/tracks/#input-format
Note: current does not consider comments
*/
std::vector<std::vector<bool> > get_adj_matrix();


/*
Scans in a graph and returns it as an adjancy list. In particular, index i in the returned vector contains the edges adjacent to i, in sorted order
Uses 1-indexing, so index 0 should be ignored
Uses this input format https://pacechallenge.org/2021/tracks/#input-format 
*/
std::vector<std::vector<int> > get_adj_list();


/*
Takes in two adjacency matrices (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_matrix(std::vector<std::vector<bool> > initial, std::vector<std::vector<bool> > final);


/*
Takes in two adjacency lists (first being the original graph, second being modified), prints out the difference
Uses this output format https://pacechallenge.org/2021/tracks/#output-format-for-exact-track-and-heuristic-track
*/
void print_from_adj_list(std::vector<std::vector<int> > initial, std::vector<std::vector<int> > final);