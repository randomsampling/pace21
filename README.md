# README #

Submission to the heuristic track of the 2021 PACE Challenge, by the Random Sampling group of UNSW GraphAbility VIP project.

The code is in C++, and no external libraries are required. The code we submitted is in one_file.cpp. The submission uses code from one-at-a-time-rand.cpp, sparse/triangles-random.cpp and input.cpp. The remaining code is our concepts/ideas/trial-and-error.
